<?php
require 'vendor/autoload.php';
use Omnipay\Omnipay;

$gateway = Omnipay::create('PayPal_Express');

$gateway->setUsername('username');
$gateway->setPassword('password');
$gateway->setSignature('signature');


$gateway->setTestMode(true);

$response = $gateway->purchase(
    array(
            'cancelUrl'=>'https://simplon.co/',
            'returnUrl'=>'http://localhost:8001/succes.php',
            'description'=>'test',
            'amount'=>24.00,
            'currency'=>'EUR'
    )
)->send();

$response->redirect();
?>
